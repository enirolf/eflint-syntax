# eFLINT-syntax

This repository contains various configurations for supporting the syntax of eFLINT.

## LaTeX

File: `eflint_listings.tex`

Just include it using `\input` at the start of the document.
And then the listings you write are formatted as eFLINT (for which you must use the `listings` package).

## Sublime Text

File: `eFLINT.sublime-syntax`

Create a symbolic link or copy the file to the `Packages/User` directory of your Sublime installation. It depends on your system where this directory is stored. Most likely it is in the following locations:
 * macOS: ~/Library/Application Support/Sublime Text/Packages/User
 * Windows: %APPDATA%\Sublime Text/Packages/User
 * Linux: ~/.config/sublime-text/Packages/User
